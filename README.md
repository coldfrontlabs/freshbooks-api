
# Freshbooks API

API library for the v2 Freshbooks API (not Freshbooks Classic).


Example usage.

```php
use Freshbooks\Client;


// Create the Client connection and authorize the user.
$freshbooks = Client::create(CLIENT_ID, CLIENT_SECRET, REDIRECT_URL)->authorize();

// Example get the list of clients with a search filter applied.
$freshbooks->clients()->getAll(['search' => ['email' => '123fake@example.com', 'userid' => '12345']]);

// Fetch all the invoices for a specific custom and include the line items.
$freshbooks->invoices()->getAll(['search' => ['customerid' => '54321'], 'include' => ['lines']]);

// Fetch a single estimate by ID.
$freshbooks->estimates()->get('123442');

// Create a new invoice.
$freshbooks->invoices()->create(['invoice' => ['email' => '123fake@example.com', 'customerid' => 12344, 'create_date' => '2018-02-02']]);

```
