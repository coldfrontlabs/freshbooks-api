<?php

namespace Freshbooks;

use Freshbooks\Api\TimeEntry;
use Freshbooks\Api\Payments;
use Freshbooks\Api\Invoices;
use Freshbooks\Api\Items;
use Freshbooks\Api\Expenses;
use Freshbooks\Api\Estimate;
use Freshbooks\Api\Clients;
use Freshbooks\Api\Business;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use League\OAuth2\Client\Provider\GenericProvider;
use League\OAuth2\Client\Token\AccessToken;

/**
 * Client connection class.
 */
class Client {

  /**
   * Client id string.
   *
   * @var string
   */
  private $clientId;

  /**
   * Client secret.
   *
   * @var string
   */
  private $clientSecret;

  /**
   * Redirect URI.
   *
   * @var string
   */
  private $redirectUri;

  /**
   * Freshbooks API url.
   *
   * @var string
   */
  private $freshbooksApiUrl;

  /**
   * Access token.
   *
   * @var string
   */
  private $accessToken;

  /**
   * Provider.
   *
   * @var string
   */
  private $provider;

  /**
   * Reference to me.
   *
   * @var object
   */
  private $me;

  /**
   * Client constructor.
   *
   * @param string $clientId
   *   Client ID.
   * @param string $clientSecret
   *   Client secret.
   * @param string $redirectUri
   *   Redirect URI after OAuth connection.
   * @param string $api_url
   *   API url.
   */
  public function __construct(string $clientId, string $clientSecret, string $redirectUri, string $api_url = 'https://api.freshbooks.com') {
    // Create an instance of the freshbooks object.
    $this->setUrl($api_url);
    $this->clientId = $clientId;
    $this->clientSecret = $clientSecret;
    $this->redirectUri = $redirectUri;

    $this->provider = new GenericProvider([
    // The client ID assigned to you by the provider.
      'clientId'                => $this->clientId,
    // The client password assigned to you by the provider.
      'clientSecret'            => $this->clientSecret,
      'redirectUri'             => $this->redirectUri,
      'urlAuthorize'            => 'https://my.freshbooks.com/service/auth/oauth/authorize',
      'urlAccessToken'          => 'https://api.freshbooks.com/auth/oauth/token',
      'urlResourceOwnerDetails' => $this->freshbooksApiUrl . '/auth/api/v1/users/me',
    ]);
  }

  /**
   * Create new client connection.
   *
   * @param string $clientId
   *   Client ID.
   * @param string $clientSecret
   *   Client secret.
   * @param string $redirectUri
   *   Redirect URI after OAuth connection.
   *
   * @return object
   *   Returns a new client object.
   */
  public static function create(string $clientId, string $clientSecret, string $redirectUri) {
    return new self($clientId, $clientSecret, $redirectUri);
  }

  /**
   * Authorize the library to make API requests.
   */
  public function authorize() {
    try {
      $code = $state = NULL;
      if (isset($_GET['code'])) {
        $code = $_GET['code'];
      }
      if (isset($_GET['state'])) {
        $state = $_GET['state'];
      }

      if ($code === NULL) {
        $authUrl = $this->provider->getAuthorizationUrl();
        $_SESSION['oauth2state'] = $this->provider->getState();
        header('Location: ' . $authUrl);
        exit;
      }
      // Check given state against previously stored
      // one to mitigate CSRF attack.
      elseif (empty($state) || ($state !== $_SESSION['oauth2state'])) {
        unset($_SESSION['oauth2state']);
        exit('Invalid state');
      }
      else {
        if (empty($this->accessToken)) {
          $this->accessToken = $this->provider->getAccessToken('authorization_code', ['code' => $code]);
        }

        $this->me = $this->provider->getResourceOwner($this->accessToken);
      }
      return $this;
    }
    catch (IdentityProviderException $e) {
      // TODO: catch the exception and throw an error.
      exit($e->getMessage());
    }
  }

  /**
   * Wrapper function to make a get request.
   *
   * @param string $path
   *   Path to make the request on.
   * @param mixed $parameters
   *   Optional parameters to pass onto the request method.
   */
  public function get($path, $parameters = []) {
    $path .= $this->queryFilters($parameters);
    $response = $this->request('GET', $this->freshbooksApiUrl . $path, $parameters);
    return $response;
  }

  /**
   * Wrapper function to make a post request.
   *
   * @param string $path
   *   Path to make the request on.
   * @param mixed $parameters
   *   Optional parameters to pass onto the request method.
   */
  public function post($path, $parameters = []) {
    $this->request('POST', $this->freshbooksApiUrl . $path, $parameters);
  }

  /**
   * Wrapper function to make a put request.
   *
   * @param string $path
   *   Path to make the request on.
   * @param mixed $parameters
   *   Optional parameters to pass onto the request method.
   */
  public function put($path, $parameters = []) {
    $this->request('PUT', $this->freshbooksApiUrl . $path, $parameters);
  }

  /**
   * Wrapper function to make a delete request.
   *
   * @param string $path
   *   Path to make the request on.
   * @param mixed $parameters
   *   Optional parameters to pass onto the request method.
   */
  public function delete($path, $parameters = []) {
    $this->request('DELETE', $this->freshbooksApiUrl . $path, $parameters);
  }

  /**
   * Request wrapper function.
   *
   * Handles refresh token requests automatically.
   */
  public function request($method, $url, array $options = []) {
    if ($this->accessToken->hasExpired()) {
      $newAccessToken = $this->provider->getAccessToken('refresh_token', [
        'refresh_token' => $this->accessToken->getRefreshToken(),
      ]);
      // @todo validate the request was successfull.
      $this->accessToken = $newAccessToken;
    }

    // Set default headers.
    // @see https://www.freshbooks.com/api/start
    $options += [
      'Content-Type' => 'application/json',
      'Api-Version' => 'alpha',
    ];

    $request = $this->provider->getAuthenticatedRequest(
      $method,
      $url,
      $this->accessToken,
      $options
    );
    return $this->provider->getParsedResponse($request);
  }

  /**
   * Function to return the oAuth access token.
   */
  public function getAccessToken() {
    return $this->accessToken;
  }

  /**
   * Function to set the oAuth access token.
   *
   * @param array $options
   *   E.g.
   *   access_token is required in this array.
   *   Optional items are
   *      - resource_owner_id
   *      - refresh_token
   *      - expires_in.
   */
  public function setAccessToken(array $options) {
    $this->accessToken = new AccessToken($options);
  }

  /**
   * Set the url to the freshbooks API.
   *
   * @param string $url
   *   Set the current Freshbooks URL.
   */
  public function setUrl($url) {
    $this->freshbooksApiUrl = $url;
  }

  /**
   * Fetch the current users information.
   *
   * @return class
   *   Return the current user
   */
  public function getMe() {
    if (empty($this->me)) {
      $this->me = $this->provider->getResourceOwner($this->accessToken);
    }
    return $this->me->toArray();
  }

  /**
   * Query builder for the search and filters.
   *
   * @param array $parameters
   *   Search filters to build out the query needed.
   *
   * @return string
   *   returns the formated string to append to the path.
   */
  public function queryFilters(array $parameters) {
    $query = '';
    foreach ($parameters as $key => $value) {
      if (empty($query)) {
        $query .= '?';
      }
      switch ($key) {
        case 'search':
          foreach ($value as $search_item => $search_value) {
            if (strlen($query) > 1) {
              $query .= "&";
            }
            $query .= urlencode("search[$search_item]") . "=" . urlencode($search_value);
          }
        break;
        case 'per_page':
        case 'page':
          if (strlen($query) > 1) {
            $query .= "&";
          }
          $query .= $key ."=" . $parameters[$key];
        break;
        case 'include':
          foreach ($value as $included_item) {
            if (strlen($query) > 1) {
              $query .= "&";
            }
            $query .= urlencode("include[]") . "=" . $included_item;
          }
        break;
        case 'query':
          foreach ($value as $item_key => $item_value) {
            if (strlen($query) > 1) {
              $query .= "&";
            }
            $query .= $item_key . '=' . urlencode($item_value);
          }
        break;
      }
    }
    return $query;
  }

  /**
   * Return a business class.
   *
   * @return \Api\Business
   *   Returns a business object.
   */
  public function business() {
    return new Business($this);
  }

  /**
   * Return a Client class.
   *
   * @return \Api\Client
   *   Returns a client object.
   */
  public function clients() {
    return new Clients($this);
  }

  /**
   * Return a estimate class.
   *
   * @return \Api\Estimate
   *   Returns an estimate object.
   */
  public function estimates() {
    return new Estimate($this);
  }

  /**
   * Return a expense class.
   *
   * @return \Api\Expenses
   *   Returns an expense object.
   */
  public function expenses() {
    return new Expenses($this);
  }

  /**
   * Return a invoices class.
   *
   * @return \Api\Invoices
   *   Returns an invoices object.
   */
  public function invoices() {
    return new Invoices($this);
  }

  /**
   * Return a items class.
   *
   * @return \Api\Items
   *   Returns an items object.
   */
  public function items() {
    return new Items($this);
  }

  /**
   * Return a payments class.
   *
   * @return \Api\Payments
   *   Returns a payments object.
   */
  public function payments() {
    return new Payments($this);
  }

  /**
   * Return a time entry class.
   *
   * @return \Api\TimeEntry
   *   Returns a time entry object.
   */
  public function timeEntries() {
    return new TimeEntry($this);
  }

}
