<?php

namespace Freshbooks\Api;

/**
 * Freshbooks Business Roles and Identities.
 *
 * @see https://www.freshbooks.com/api/me_endpoint.
 */
class Business extends AbstractApi {

  /**
   * Current business object.
   *
   * @var currentBusiness
   */
  private $currentBusiness;

  /**
   * Set the active scope for business requests.
   */
  public function setCurrentBusiness(int $id = NULL) {
    if (is_null($id)) {
      // Default to first business.
      $this->currentBusiness = $this->getBusiness();
    }
  }

  /**
   * Get the ID of the current business.
   */
  public function getCurrentBusinessId() {
    return $this->currentBusiness->id;
  }

  /**
   * Get the defined business.
   *
   * Get the desired business. Defaults to first
   * business in the $me variable.
   *
   * @param int $id
   *   The business ID.
   *
   * @return mixed
   *   Returns the business object, FALSE otherwise.
   */
  public function getBusiness(int $id = NULL) {
    // Load the default membership.
    $response = $this->client->getMe();
    if (isset($response['response'])) {
      if (is_null($id)) {
        return $response['response']['business_memberships'][0]['business'];
      }
      else {
        foreach ($response['response']['business_memberships'] as $membership) {
          if ($id == $membership['business']['id']) {
            return $membership['business'];
          }
        }
      }
    }

    return FALSE;
  }

  /**
   * Return all business memberships for the given user.
   */
  public function getBusinessMemberships() {
    $return = NULL;
    $response = $this->client->getMe();
    if (isset($response['response'])) {
      $return = $response['response']['business_memberships'];
    }
    return $return;
  }

  /**
   * Get the defined business.
   *
   * Get the desired business. Defaults to first
   * business in the $me variable.
   *
   * @param int $id
   *   The business ID.
   *
   * @return mixed
   *   Returns the business object, FALSE otherwise.
   */
  public function getBusinessMembership(int $id = NULL) {
    // Load the default membership.
    if (is_null($id)) {
      return $this->me->business_memberships[0];
    }
    else {
      foreach ($this->me->business_memberships as $membership) {
        if ($id == $membership->id) {
          return $membership;
        }
      }
    }

    return FALSE;
  }

}
