<?php

namespace Freshbooks\Api;

use Freshbooks\Client;

use Http\Discovery\StreamFactoryDiscovery;
use Http\Message\StreamFactory;

/**
 * Abstract class for Api classes.
 */
abstract class AbstractApi implements ApiInterface {

  /**
   * The account ID for this account.
   *
   * @var string
   */
  protected $accountId;

  /**
   * The client.
   *
   * @var \Freshbooks\Client
   */
  protected $client;

  /**
   * Private stream factory.
   *
   * @var \Http\Message\StreamFactory
   */
  private $streamFactory;

  /**
   * Main constructor.
   *
   * @param \Freshbooks\Client $client
   *   Main client connection object.
   * @param \Http\Message\StreamFactory|null $streamFactory
   *   Stream factory.
   */
  public function __construct(Client $client, StreamFactory $streamFactory = NULL) {
    $this->client = $client;
    $this->streamFactory = $streamFactory ?: StreamFactoryDiscovery::find();
    $account = $this->client->getMe();

    // Check to see if the roles have been set
    // so that we can fetch the account ID.
    if (isset($account['response']['roles']) && !empty($account['response']['roles'])) {
      foreach ($account['response']['roles'] as $roles) {
        // TODO: Write some logic in when multiple roles are detected?
        switch ($roles['role']) {
          case 'admin':
          case 'staff':
          case 'client':
          case 'business_employee':
            $this->accountId = $roles['accountid'];
            break;
        }
      }
    }
  }

  /**
   * Get the specified user's account base path.
   *
   * @param string $account_id
   *   Account id.
   *
   * @return string
   *   Returns the account path string.
   */
  protected function getAccountPath(string $account_id = NULL) {
    if ($account_id == NULL && $this->accountId == NULL) {

    }
    elseif ($account_id == NULL) {
      return '/accounting/account/' . $this->accountId . '/';
    }
    else {
      return '/accounting/account/' . $account_id . '/';
    }
  }

  /**
   * Get Single Item.
   *
   * @param int $item_id
   *   The unique ID to fetch.
   * @param string $account_id
   *   The account ID.
   * @param array $parameters
   *   Optional paramters.
   *
   * @return mixed
   *   Returns the single item object, FALSE otherwise.
   */
  public function get(int $item_id, string $account_id = NULL, array $parameters = []) {
    return $this->client->get($this->getAccountPath($account_id) . get_called_class()::PATH . '/' . $item_id, $parameters);
  }

  /**
   * Fetches a list of items by a pager.
   *
   * @param array $parameters
   *   Optional list of parameters that can be passed.
   * @param string $account_id
   *   The account ID.
   *
   * @return mixed
   *   Returns the list of object, FALSE otherwise.
   */
  public function getAll(array $parameters = [], string $account_id = NULL) {
    return $this->client->get($this->getAccountPath($account_id) . get_called_class()::PATH, $parameters);
  }

  /**
   * Creates a new Item.
   *
   * @param mixes $params
   *   The values used to create a new Item.
   * @param string $account_id
   *   The account ID.
   *
   * @return mixed
   *   Returns the object, FALSE otherwise.
   */
  public function create(mixes $params, string $account_id = NULL) {
    return $this->client->post($this->getAccountPath($account_id) . get_called_class()::PATH, $params);
  }

  /**
   * Updates an existing Item.
   *
   * @param int $item_id
   *   The Unique ID.
   * @param mixes $params
   *   The parameters to update the client with.
   * @param string $account_id
   *   The account ID.
   *
   * @return mixed
   *   Returns the client object, FALSE otherwise.
   */
  public function update(int $item_id, mixes $params, string $account_id = NULL) {
    return $this->client->put($this->getAccountPath($account_id) . get_called_class()::PATH . '/' . $item_id, $params);
  }

  /**
   * Delete an Item.
   *
   * @param int $item_id
   *   The unique item ID.
   * @param string $account_id
   *   The account ID.
   *
   * @return mixed
   *   Returns the TRUE on success or FALSE otherwise.
   */
  public function delete(int $item_id, string $account_id = NULL) {
    return $this->client->delete($this->getAccountPath($account_id) . get_called_class()::PATH . '/' . $item_id);
  }

}
