<?php

namespace Freshbooks\Api;

/**
 * See the following URL for the API for the freshbooks Items.
 *
 * Https://www.freshbooks.com/api/items.
 */
class Items extends AbstractApi {

  const PATH = 'items/items';

}
