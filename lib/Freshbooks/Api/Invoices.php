<?php

namespace Freshbooks\Api;

/**
 * See the following URL for the API for the freshbooks Invoices.
 *
 * Https://www.freshbooks.com/api/invoices.
 */
class Invoices extends AbstractApi {

  const PATH = 'invoices/invoices';

}
