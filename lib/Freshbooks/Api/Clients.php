<?php

namespace Freshbooks\Api;

/**
 * See the following URL for the API for the freshbooks Client.
 *
 * Https://www.freshbooks.com/api/clients.
 */
class Clients extends AbstractApi {

  const PATH = 'users/clients';

}
