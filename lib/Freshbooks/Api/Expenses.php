<?php

namespace Freshbooks\Api;

/**
 * See the following URL for the API for the freshbooks Expense.
 *
 * Https://www.freshbooks.com/api/expenses.
 */
class Expenses extends AbstractApi {

  const PATH = 'expenses/expenses';

}
