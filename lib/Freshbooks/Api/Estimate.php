<?php

namespace Freshbooks\Api;

/**
 * See the following URL for the API for the freshbooks Client.
 *
 * Https://www.freshbooks.com/api/estimates.
 */
class Estimate extends AbstractApi {

  const PATH = 'estimates/estimates';

}
