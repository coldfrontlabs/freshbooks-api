<?php

namespace Freshbooks\Api;

use Freshbooks\Client;

/**
 * Api interface.
 */
interface ApiInterface {

  /**
   * Constructor.
   */
  public function __construct(Client $client);

}
