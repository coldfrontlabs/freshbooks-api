<?php

namespace Freshbooks\Api;

/**
 * See the following URL for the API for the freshbooks Payment.
 *
 * Https://www.freshbooks.com/api/payments.
 */
class Payments extends AbstractApi {

  const PATH = 'payments/payments';

}
