<?php

namespace Freshbooks\Api;

use Freshbooks\Client;

use Http\Discovery\StreamFactoryDiscovery;
use Http\Message\StreamFactory;

  
/**
 * Time entry class.
 */
class TimeEntry {

  /**
   * The client.
   *
   * @var \Freshbooks\Client
   */
  protected $client;


  /**
   * Private stream factory.
   *
   * @var \Http\Message\StreamFactory
   */
  private $streamFactory;

  public function __construct(Client $client, StreamFactory $streamFactory = NULL) {
    $this->client = $client;
    $this->streamFactory = $streamFactory ?: StreamFactoryDiscovery::find();
  }

  /**
   * Get Single Time entry.
   *
   * @param string $business_id
   *   The business ID.
   * @param int $time_entry_id
   *   The time entry ID.
   *
   * @return mixed
   *   Returns the Time entry object, FALSE otherwise.
   */
  public function get(string $business_id, int $time_entry_id) {
    return $this->client->get('/timetracking/business/' . $business_id . '/time_entries/' . $time_entry_id);
  }

  /**
   * Fetches a list of time entries by a pager.
   *
   * @param string $business_id
   *   The business ID.
   * @param mixes $params
   *   Parameters used to pass to the get function.
   *
   * @return mixed
   *   Returns the client object, FALSE otherwise.
   */
  public function getAll(string $business_id, $params) {
    return $this->client->get('/timetracking/business/' . $business_id . '/time_entries', $params);
  }

  /**
   * Creates a new time entry.
   *
   * @param string $business_id
   *   The business ID.
   * @param mixes $params
   *   The values used to create a new time entry.
   *
   * @return mixed
   *   Returns the time entry object, FALSE otherwise.
   */
  public function create(string $business_id, $params) {
    return $this->client->post('/timetracking/business/' . $business_id . '/time_entries', $params);
  }

  /**
   * Updates an time entry.
   *
   * @param string $business_id
   *   The business ID.
   * @param int $time_entry_id
   *   The time entry ID.
   * @param mixes $params
   *   The parameters to update the time entry with.
   *
   * @return mixed
   *   Returns the time entry object, FALSE otherwise.
   */
  public function update(string $business_id, int $time_entry_id, $params) {
    return $this->client->put('/timetracking/business/' . $business_id . '/time_entries/' . $time_entry_id, $params);
  }

  /**
   * Delete a time entry.
   *
   * @param string $business_id
   *   The business ID.
   * @param int $time_entry_id
   *   The time entry ID.
   *
   * @return mixed
   *   Returns the TRUE on success or FALSE otherwise.
   */
  public function delete(string $business_id, int $time_entry_id) {
    return $this->client->delete('/timetracking/business/' . $business_id . '/time_entries/' . $time_entry_id);
  }

}
